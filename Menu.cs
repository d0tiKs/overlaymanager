﻿using System;
using System.IO;
using System.Windows.Forms;

namespace OverlayManager
{
    public partial class Menu : Form
    {
        private bool _visible = false;
        private Overlay _overlay;
        private int _screenIndex = 0;
        private Screen _screen;

        public Menu()
        {
            InitializeComponent();
            _screen = Screen.AllScreens[_screenIndex];
            _overlay = new Overlay(_screen);

            UpdateMenu(true);
            UpdateOverlay();
        }

        #region Events

        private void tsmiFileNew_Click(object sender, EventArgs e)
        {
            BrowseFile();
        }

        private void pbPreview_Click(object sender, EventArgs e)
        {
            BrowseFile();
        }

        private void btHide_Click(object sender, EventArgs e)
        {
            ToggleVisiblity();
        }

        private void btScreen_Click(object sender, EventArgs e)
        {
            ChangeScreen();
            UpdateMenu();
        }

        private void tbXAxis_Scroll(object sender, EventArgs e)
        {
            UpdateSlider(x: tbXAxis.Value);
        }

        private void tbYAxis_Scroll(object sender, EventArgs e)
        {
            UpdateSlider(y: tbYAxis.Value);
        }

        private void tbOpacity_Scroll(object sender, EventArgs e)
        {
            UpdateSlider(opacity: tbOpacity.Value);
        }

        private void nupXAxis_ValueChanged(object sender, EventArgs e)
        {
            UpdateSlider(x: (int)nupXAxis.Value);
        }

        private void nupYAxis_ValueChanged(object sender, EventArgs e)
        {
            UpdateSlider(y: (int)nupYAxis.Value);

        }

        private void nupOpacity_ValueChanged(object sender, EventArgs e)
        {
            UpdateSlider(opacity: (int)nupOpacity.Value);
        }


        private void btCenter_Click(object sender, EventArgs e)
        {
            CenterOverlay();
        }

        private void tsmiOverlayScreen_Click(object sender, EventArgs e)
        {
            ChangeScreen();
            UpdateMenu();
        }

        private void tsmiOverlayVisibility_Click(object sender, EventArgs e)
        {
            ToggleVisiblity();
        }

        #endregion

        private void BrowseFile()
        {
            var fileDial = new OpenFileDialog();
            fileDial.Filter = "Transparent Image (*.png,*.gif)|*.png;*.gif|All files (*.*)|*.*";
            fileDial.Title = "Choose Crosshair";
            fileDial.CheckPathExists = true;
            fileDial.Multiselect = false;
            fileDial.InitialDirectory = Directory.GetCurrentDirectory();

            if (fileDial.ShowDialog() == DialogResult.OK)
            {
                tbPath.Text = fileDial.FileName;
                ShowPreview(fileDial.FileName);
                CenterOverlay();
                UpdateOverlay();

            }
        }

        private void ShowPreview(string path)
        {
            pbPreview.ImageLocation = path;
            pbPreview.Refresh();
        }

        private void ToggleVisiblity()
        {
            _visible = !_visible;
            if (_visible)
            {
                btVisibility.Text = "Hide";
                tsmiOverlayVisibility.Text = "Visibility (on)";
                UpdateOverlay();
                _overlay.Show();
            }
            else
            {
                btVisibility.Text = "Show";
                tsmiOverlayVisibility.Text = "Visibility (off)";
                _overlay.Hide();
            }
        }

        private void ChangeScreen(int? index = null)
        {
            if (index.HasValue)
            {
                _screenIndex = index.Value;
            }
            else
            {
                ++_screenIndex;
            }

            _screenIndex %= (Screen.AllScreens.Length);
            _screen = Screen.AllScreens[_screenIndex];
            _overlay.MoveToScreen(_screenIndex);
            UpdateOverlay();
        }

        private void UpdateOverlay()
        {
            if (_overlay is null)
            {
                return;
            }

            _overlay.MoveOverlay(tbXAxis.Value, tbYAxis.Value);

            if (pbPreview.Image is null)
            {
                return;
            }

            _overlay.ChangePicture(pbPreview.Image, (int)nupOpacity.Value);
        }

        public void CenterOverlay()
        {
            int x;
            int y;

            x = _screen.WorkingArea.Width / 2;
            y = _screen.WorkingArea.Height / 2;

            if (pbPreview.Image != null)
            {
                x -= pbPreview.Image.Width / 2;
                y -= pbPreview.Image.Height / 2;
            }

            nupXAxis.Value = tbXAxis.Value = x;
            nupYAxis.Value = tbYAxis.Value = y;

            _overlay.MoveOverlay(x, y);
        }

        private void UpdateSlider(int? x = null, int? y = null, int? opacity = null)
        {
            if (x.HasValue)
            {
                nupXAxis.Value = tbXAxis.Value = x.Value;
            }

            if (y.HasValue)
            {
                nupYAxis.Value = tbYAxis.Value = y.Value;
            }

            if (opacity.HasValue)
            {
                nupOpacity.Value = tbOpacity.Value = opacity.Value;
            }

            UpdateOverlay();
        }

        private void UpdateMenu(bool change = false)
        {
            nupXAxis.Maximum = tbXAxis.Maximum = _screen.Bounds.Width;
            nupYAxis.Maximum = tbYAxis.Maximum = _screen.Bounds.Height;

            tbXAxis.TickFrequency = tbXAxis.Maximum / 10;
            tbYAxis.TickFrequency = tbYAxis.Maximum / 10;

            if (!change)
            {
                return;
            }

            nupXAxis.Value = tbXAxis.Value = _screen.Bounds.Width / 2;
            nupYAxis.Value = tbYAxis.Value = _screen.Bounds.Height / 2;
        }
    }
}
