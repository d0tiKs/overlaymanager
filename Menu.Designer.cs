﻿namespace OverlayManager
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPath = new System.Windows.Forms.TextBox();
            this.tbXAxis = new System.Windows.Forms.TrackBar();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.lbXAxis = new System.Windows.Forms.Label();
            this.nupXAxis = new System.Windows.Forms.NumericUpDown();
            this.nupYAxis = new System.Windows.Forms.NumericUpDown();
            this.lbYAxis = new System.Windows.Forms.Label();
            this.tbYAxis = new System.Windows.Forms.TrackBar();
            this.nupOpacity = new System.Windows.Forms.NumericUpDown();
            this.lbOpacity = new System.Windows.Forms.Label();
            this.tbOpacity = new System.Windows.Forms.TrackBar();
            this.btVisibility = new System.Windows.Forms.Button();
            this.btScreen = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiFIle = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOverlay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOverlayScreen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOverlayVisibility = new System.Windows.Forms.ToolStripMenuItem();
            this.btCenter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbXAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupXAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupYAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbYAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupOpacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOpacity)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(12, 26);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(436, 20);
            this.tbPath.TabIndex = 1;
            // 
            // tbXAxis
            // 
            this.tbXAxis.Location = new System.Drawing.Point(214, 67);
            this.tbXAxis.Name = "tbXAxis";
            this.tbXAxis.Size = new System.Drawing.Size(150, 45);
            this.tbXAxis.TabIndex = 3;
            this.tbXAxis.Scroll += new System.EventHandler(this.tbXAxis_Scroll);
            // 
            // pbPreview
            // 
            this.pbPreview.Location = new System.Drawing.Point(12, 52);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(200, 200);
            this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbPreview.TabIndex = 6;
            this.pbPreview.TabStop = false;
            this.pbPreview.Click += new System.EventHandler(this.pbPreview_Click);
            // 
            // lbXAxis
            // 
            this.lbXAxis.AutoSize = true;
            this.lbXAxis.Location = new System.Drawing.Point(219, 52);
            this.lbXAxis.Name = "lbXAxis";
            this.lbXAxis.Size = new System.Drawing.Size(36, 13);
            this.lbXAxis.TabIndex = 7;
            this.lbXAxis.Text = "X Axis";
            // 
            // nupXAxis
            // 
            this.nupXAxis.Location = new System.Drawing.Point(373, 67);
            this.nupXAxis.Name = "nupXAxis";
            this.nupXAxis.Size = new System.Drawing.Size(75, 20);
            this.nupXAxis.TabIndex = 11;
            this.nupXAxis.ValueChanged += new System.EventHandler(this.nupXAxis_ValueChanged);
            // 
            // nupYAxis
            // 
            this.nupYAxis.Location = new System.Drawing.Point(373, 118);
            this.nupYAxis.Name = "nupYAxis";
            this.nupYAxis.Size = new System.Drawing.Size(75, 20);
            this.nupYAxis.TabIndex = 14;
            this.nupYAxis.ValueChanged += new System.EventHandler(this.nupYAxis_ValueChanged);
            // 
            // lbYAxis
            // 
            this.lbYAxis.AutoSize = true;
            this.lbYAxis.Location = new System.Drawing.Point(219, 103);
            this.lbYAxis.Name = "lbYAxis";
            this.lbYAxis.Size = new System.Drawing.Size(36, 13);
            this.lbYAxis.TabIndex = 13;
            this.lbYAxis.Text = "Y Axis";
            // 
            // tbYAxis
            // 
            this.tbYAxis.Location = new System.Drawing.Point(214, 118);
            this.tbYAxis.Name = "tbYAxis";
            this.tbYAxis.Size = new System.Drawing.Size(150, 45);
            this.tbYAxis.TabIndex = 12;
            this.tbYAxis.Scroll += new System.EventHandler(this.tbYAxis_Scroll);
            // 
            // nupOpacity
            // 
            this.nupOpacity.Location = new System.Drawing.Point(373, 169);
            this.nupOpacity.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nupOpacity.Name = "nupOpacity";
            this.nupOpacity.Size = new System.Drawing.Size(75, 20);
            this.nupOpacity.TabIndex = 17;
            this.nupOpacity.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nupOpacity.ValueChanged += new System.EventHandler(this.nupOpacity_ValueChanged);
            // 
            // lbOpacity
            // 
            this.lbOpacity.AutoSize = true;
            this.lbOpacity.Location = new System.Drawing.Point(219, 154);
            this.lbOpacity.Name = "lbOpacity";
            this.lbOpacity.Size = new System.Drawing.Size(43, 13);
            this.lbOpacity.TabIndex = 16;
            this.lbOpacity.Text = "Opacity";
            // 
            // tbOpacity
            // 
            this.tbOpacity.Location = new System.Drawing.Point(214, 169);
            this.tbOpacity.Maximum = 255;
            this.tbOpacity.Name = "tbOpacity";
            this.tbOpacity.Size = new System.Drawing.Size(150, 45);
            this.tbOpacity.TabIndex = 15;
            this.tbOpacity.TickFrequency = 17;
            this.tbOpacity.Value = 255;
            this.tbOpacity.Scroll += new System.EventHandler(this.tbOpacity_Scroll);
            // 
            // btVisibility
            // 
            this.btVisibility.Location = new System.Drawing.Point(379, 228);
            this.btVisibility.Name = "btVisibility";
            this.btVisibility.Size = new System.Drawing.Size(75, 23);
            this.btVisibility.TabIndex = 9;
            this.btVisibility.Text = "Show";
            this.btVisibility.UseVisualStyleBackColor = true;
            this.btVisibility.Click += new System.EventHandler(this.btHide_Click);
            // 
            // btScreen
            // 
            this.btScreen.Location = new System.Drawing.Point(298, 228);
            this.btScreen.Name = "btScreen";
            this.btScreen.Size = new System.Drawing.Size(75, 23);
            this.btScreen.TabIndex = 18;
            this.btScreen.Text = "Screen";
            this.btScreen.UseVisualStyleBackColor = true;
            this.btScreen.Click += new System.EventHandler(this.btScreen_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFIle,
            this.tsmiOverlay});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(461, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiFIle
            // 
            this.tsmiFIle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFileNew,
            this.tsmiFileSave,
            this.tsmiFileOpen});
            this.tsmiFIle.Name = "tsmiFIle";
            this.tsmiFIle.Size = new System.Drawing.Size(37, 20);
            this.tsmiFIle.Text = "File";
            // 
            // tsmiFileNew
            // 
            this.tsmiFileNew.Name = "tsmiFileNew";
            this.tsmiFileNew.Size = new System.Drawing.Size(103, 22);
            this.tsmiFileNew.Text = "New";
            this.tsmiFileNew.Click += new System.EventHandler(this.tsmiFileNew_Click);
            // 
            // tsmiFileSave
            // 
            this.tsmiFileSave.Enabled = false;
            this.tsmiFileSave.Name = "tsmiFileSave";
            this.tsmiFileSave.Size = new System.Drawing.Size(180, 22);
            this.tsmiFileSave.Text = "Save";
            // 
            // tsmiFileOpen
            // 
            this.tsmiFileOpen.Enabled = false;
            this.tsmiFileOpen.Name = "tsmiFileOpen";
            this.tsmiFileOpen.Size = new System.Drawing.Size(180, 22);
            this.tsmiFileOpen.Text = "Open";
            // 
            // tsmiOverlay
            // 
            this.tsmiOverlay.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOverlayScreen,
            this.tsmiOverlayVisibility});
            this.tsmiOverlay.Name = "tsmiOverlay";
            this.tsmiOverlay.Size = new System.Drawing.Size(59, 20);
            this.tsmiOverlay.Text = "Overlay";
            // 
            // tsmiOverlayScreen
            // 
            this.tsmiOverlayScreen.Name = "tsmiOverlayScreen";
            this.tsmiOverlayScreen.Size = new System.Drawing.Size(144, 22);
            this.tsmiOverlayScreen.Text = "Screen";
            this.tsmiOverlayScreen.Click += new System.EventHandler(this.tsmiOverlayScreen_Click);
            // 
            // tsmiOverlayVisibility
            // 
            this.tsmiOverlayVisibility.Name = "tsmiOverlayVisibility";
            this.tsmiOverlayVisibility.Size = new System.Drawing.Size(144, 22);
            this.tsmiOverlayVisibility.Text = "Visibility (off)";
            this.tsmiOverlayVisibility.Click += new System.EventHandler(this.tsmiOverlayVisibility_Click);
            // 
            // btCenter
            // 
            this.btCenter.Location = new System.Drawing.Point(217, 228);
            this.btCenter.Name = "btCenter";
            this.btCenter.Size = new System.Drawing.Size(75, 23);
            this.btCenter.TabIndex = 20;
            this.btCenter.Text = "Center";
            this.btCenter.UseVisualStyleBackColor = true;
            this.btCenter.Click += new System.EventHandler(this.btCenter_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 264);
            this.Controls.Add(this.btCenter);
            this.Controls.Add(this.btScreen);
            this.Controls.Add(this.nupOpacity);
            this.Controls.Add(this.lbOpacity);
            this.Controls.Add(this.tbOpacity);
            this.Controls.Add(this.nupYAxis);
            this.Controls.Add(this.lbYAxis);
            this.Controls.Add(this.tbYAxis);
            this.Controls.Add(this.nupXAxis);
            this.Controls.Add(this.btVisibility);
            this.Controls.Add(this.lbXAxis);
            this.Controls.Add(this.pbPreview);
            this.Controls.Add(this.tbXAxis);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Menu";
            this.Text = "Crosshair Menu";
            ((System.ComponentModel.ISupportInitialize)(this.tbXAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupXAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupYAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbYAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupOpacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOpacity)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.TrackBar tbXAxis;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.Label lbXAxis;
        private System.Windows.Forms.NumericUpDown nupXAxis;
        private System.Windows.Forms.NumericUpDown nupYAxis;
        private System.Windows.Forms.Label lbYAxis;
        private System.Windows.Forms.TrackBar tbYAxis;
        private System.Windows.Forms.NumericUpDown nupOpacity;
        private System.Windows.Forms.Label lbOpacity;
        private System.Windows.Forms.TrackBar tbOpacity;
        private System.Windows.Forms.Button btVisibility;
        private System.Windows.Forms.Button btScreen;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiFIle;
        private System.Windows.Forms.ToolStripMenuItem tsmiFileNew;
        private System.Windows.Forms.ToolStripMenuItem tsmiFileOpen;
        private System.Windows.Forms.ToolStripMenuItem tsmiFileSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiOverlay;
        private System.Windows.Forms.ToolStripMenuItem tsmiOverlayScreen;
        private System.Windows.Forms.ToolStripMenuItem tsmiOverlayVisibility;
        private System.Windows.Forms.Button btCenter;
    }
}

