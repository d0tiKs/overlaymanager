﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace OverlayManager
{
    public partial class Overlay : LayeredWindow.LayeredWindow
    {
        const int GWL_EXSTYLE = -20;
        const int WS_EX_LAYERED = 0x80000;
        const int WS_EX_TRANSPARENT = 0x20;

        Screen _screen;
        Bitmap _image;
        int _opacity = 255;

        public Overlay(Screen screen = null)
        {

            InitializeComponent();

            IntPtr style = (IntPtr)(Tools.GetWindowLong(this.Handle, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_TRANSPARENT);
            Tools.SetWindowLong(this.Handle, GWL_EXSTYLE, style);

            _screen = screen ?? Screen.PrimaryScreen;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            Tools.AlwaysOnTop(this.Handle);

            if (this.BackgroundImage is null)
            {
                return;
            }

            this.Width = this.BackgroundImage.Width;
            this.Height = this.BackgroundImage.Height;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
#if DEBUG
            base.OnPaintBackground(e);
#endif
        }

        #region Events

        #endregion

        public void ChangePicture(Image image = null, int? opacity = null)
        {
            if (image != null)
            {
                _image = new Bitmap(image);
            }

            if (opacity != null)
            {
                _opacity = Tools.Clamp(opacity.Value, 0, 255);
            }

            this.SelectBitmap(_image, _opacity);
            this.Width = image.Width;
            this.Height = image.Height;
        }

        public void MoveOverlay(int x, int y)
        {
            this.Left = _screen.WorkingArea.X + x;
            this.Top = _screen.WorkingArea.Y + y;
        }

        public void MoveToScreen(int index)
        {
            _screen = Screen.AllScreens[index];
            this.Location = _screen.WorkingArea.Location;
        }
    }
}
